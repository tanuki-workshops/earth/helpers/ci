# helpers

## Code quality

```yaml
stages:
  - 🧭code-quality
  - 🐳build
  - 🚢deploy

include:
  - project: 'kubernetes/helpers'
    file: 'Code-Quality.gitlab-ci.yml'

🧐eslint:
  stage: 🧭code-quality
  extends: .quality:check:javascript
```

or without `stage: 🧭code-quality`

```yaml
include:
  - project: 'kubernetes/helpers'
    file: 'Code-Quality.gitlab-ci.yml'

#----------------------------
# Check quality with eslint
#----------------------------
🧐eslint:
  extends: .quality:check:javascript
```

### Important

you need to add 2 files:

- `.eslintrc.js`
- `.eslintrc.yml`

> there are 2 samples in this project


## Kaniko build + Kubernetes deploy

```yaml
stages:
  - 🐳build
  - 🚢deploy

variables:
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80

include:
  - project: 'kubernetes/helpers'
    file: 'Kaniko-Build.gitlab-ci.yml'
  - project: 'kubernetes/helpers'
    file: 'Kubernetes-Deploy.gitlab-ci.yml'

#----------------------------
# Build Docker image
#----------------------------
📦kaniko-build:
  extends: .kaniko:build
  stage: 🐳build

#----------------------------
# Deploy
#----------------------------
🚀production-deploy:
  extends: .production:deploy
  stage: 🚢deploy

🎉preview-deploy:
  stage: 🚢deploy
  extends: .preview:deploy
  environment:
    on_stop: 😢stop-preview-deploy

😢stop-preview-deploy:
  stage: 🚢deploy
  extends: .stop:preview:deploy
```
